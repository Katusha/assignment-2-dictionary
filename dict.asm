%include "lib.inc"
section .text
global find_word
find_word:
	xor rax, rax
	.loop:
		cmp rsi, 0
		je .end
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 1
		je .found
		mov rsi, [rsi]
		jmp .loop
	.found:
		mov rax, rsi
	.end:
		ret

