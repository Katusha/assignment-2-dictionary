%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%assign MAX_BUFFER_SIZE 255
extern find_word

section .rodata
key_not_found_message:
	db "Этот ключ не найден", 0
overflow_message:
	db "Длина строки должна быть не более чем 255 символов", 0

section .bss
buffer: resb MAX_BUFFER_SIZE db 0

section .text
global _start
_start:
	mov rdi, buffer
	mov rsi, 255
	call read_string
	cmp rax, 0
	je .buffer_overflow
	
	mov rdi, rax
	mov rsi, first_word
	call find_word
	cmp rax, 0
	je .not_found

	add rax, 8
	mov rdi, rax
	push rdi
	call string_length
	inc rax
	pop rdi
	add rdi, rax
	call print_string
	call print_newline
	jmp exit

	.buffer_overflow:
		mov rdi, overflow_message
		call print_string_to_stderr
		jmp exit
	.not_found:
		mov rdi, key_not_found_message
		call print_string_to_stderr
		jmp exit
		


