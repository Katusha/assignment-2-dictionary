ASM=nasm
ASMFLAGS=-g -felf64 -o
LD=ld
LDFLAGS=-o

program: main.o lib.o dict.o
		$(LD) $(LDFLAGS) $@ $^
%.o: %.asm
		$(ASM) $(ASMFLAGS) $@ $<
.PHONY: clean
clean:
	rm -rf *.o

